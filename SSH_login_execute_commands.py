#author:Shahnawaz
#=========================

import sys, os, string 
import paramiko
from openpyxl import load_workbook
import threading

ex = load_workbook(os.path.abspath(os.path.join(os.path.dirname(__file__),'path_to_your_excel')))
sheet = ex['sheet_name'];
c = sheet['block_value'].value
str_c = str(c)
print ("\nReq id " + c + '\n')

#Method to connect to servers with SSH and perform commands execution

def open_ssh_conn(ip):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(ip,username = 'username',key_filename='path_to_your_private_key')
    connection = ssh.invoke_shell()
    print ("\nConnecting to {} using SSH\n".format(ip))
    
    output = connection.recv(6000)
    cmd1 = "cd /opt/bvc/controller/data/log \n\n pwd" 
    stdin, stdout, stderr = ssh.exec_command(cmd1)
    pwd = stdout.read()
    
    #sample command which i have used to search log file against a search string
    cmd2 = "cd /opt/bvc/controller/data/log \n grep -l " +"'"+str_c+"'"+" *"
    stdin, stdout, stderr = ssh.exec_command(cmd2)
    log_file = stdout.readlines()
    str_logfile=str(log_file)


    print("================================")
    print ('\n[%s] >' %ip + '\n')
    print (pwd)
    #print strlog[0]
    for log in log_file:
        print log
        
        #Write filename to Excel
        sheet['C2'].value = log
        ex.save('path_to_your_excel')
        print("Excel updated with log filename")

    
#Add your remote server ips'

#Env 1
def create_threads_Env1():
    threads = []
    host = ['host_ip1', 'host_ip2', 'host_ip3']
    for ip in host:
        th = threading.Thread(target = open_ssh_conn ,args = (ip,))
        th.start()
        threads.append(th)
    for th in threads:
        th.join()

#Env 2
def create_threads_Env2():
    threads = []
    host = ['host_ip1', 'host_ip2', 'host_ip3']
    for ip in host:
        th = threading.Thread(target = open_ssh_conn ,args = (ip,))
        th.start()
        threads.append(th)
    for th in threads:
        th.join()
 
#Select Env
def ch():
    try:

        inp = str(input("press 1 for Env 1 / press 2 for Env 2: "))

        if inp == "1":
            create_threads_Env1()
        elif inp == "2":
            create_threads_Env2()
        else:
            print ("Invalid input --> choose again")
            ch()
    except Exception as e:
        sys.exit("Invalid input format %s --> " % (str(e)))
           

if __name__ == "__main__":
        ch()
        print "Exiting the program"


