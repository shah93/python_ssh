# Python_SSH [Multi-Threaded]

##Overview
> Simple Script written in python which includes SSH login to multiple remote servers with a feature to run commands _parallelly_
> > This script can be useful in case of fetching machine logs against any request id/search string (fetched from Excel)

##Features
> SSH login to remote linux servers with private key (OpenSSH format)
>
> Choice to select hosts to connect in case of multiple Environments
>
> Pass commands which will be executing at the same time in multiple servers
